package com.trix.log;

import com.trix.util.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 07/09/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomJobLoggerTest {
    private Map m = new HashMap<>();

    /*@Test
    public void customLogMessageDatabase() throws Exception{
        m.put(Constants.KEY_MAP_USERNAME,"usuario1");
        m.put(Constants.KEY_MAP_PASSWORD,"1234");
        m.put(Constants.KEY_MAP_DBMS,"postgres");
        m.put(Constants.KEY_MAP_SERVER_NAME, "localhost");
        m.put(Constants.KEY_MAP_PORT,"8080");
        typesLogger(true,false,false,"Log Tipo Database",m);
    }
*/
    @Test
    public void customLogMessageConsole() throws Exception{
        typesLogger(false,true,false,"Log Tipo File",m);
    }

    @Test
    public void customLogMessageFileText() throws Exception{
        m.put(Constants.KEY_MAP_FILE_FOLDER, "C:/Users/admin/Desktop");
        typesLogger(false,false,true,"Log Tipo Consola",m);
    }


    private static void typesLogger(boolean typeDatabase,boolean typeConsole, boolean typeFile,String mensaje, Map params) throws Exception{
        /**
         * Graba todos los tipos Logs
         */
        CustomJobLogger job = new CustomJobLogger(typeFile,typeConsole,typeDatabase,
                true,true,true,params);
        job.CustomLogMessage("Logger mesage "+mensaje+"  - Grabando todos los tipos de mensajes");


        /**
         * Graba solo tipo Message
         */
        job = new CustomJobLogger(typeFile,typeConsole,typeDatabase,
                true,false,false,params);
        job.CustomLogMessage("Logger message "+mensaje+" - Grabando solo tipo mensaje");


        /**
         * Graba solo tipo warning
         */
        job = new CustomJobLogger(typeFile,typeConsole,typeDatabase,
                false,true,false,params);
        job.CustomLogMessage("Logger message "+mensaje+" - Grabando solo tipo warning");


        /**
         * Graba solo tipo error
         */
        job = new CustomJobLogger(typeFile,typeConsole,typeDatabase,
                false,false,true,params);
        job.CustomLogMessage("Logger message "+mensaje+" - Grabando solo tipo error");


        /**
         * Graba  tipo error y warning
         */
        job = new CustomJobLogger(typeFile,typeConsole,typeDatabase,
                false,true,true,params);
        job.CustomLogMessage("Logger mesage "+mensaje+"- Grabando  tipo error y warning ");


    }

}