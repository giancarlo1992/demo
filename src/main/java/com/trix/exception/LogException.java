package com.trix.exception;

/**
 * Created by admin on 07/09/2017.
 */
public class LogException extends RuntimeException{
    private int code;
    private int subcode;
    private String message;

    public LogException(int code, int subcode, String message) {
        super(message);
        this.code = code;
        this.subcode = subcode;
        this.message = message;
    }

    public int getSubcode() {
        return subcode;
    }

    public void setSubcode(int subcode) {
        this.subcode = subcode;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
