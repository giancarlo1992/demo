package com.trix.util;

/**
 * Created by admin on 07/09/2017.
 */
public class Constants {
    public static final Integer LOG_MESSAGE_TYPE = 1;
    public static final Integer LOG_ERROR_TYPE = 2;
    public static final Integer LOG_WARNING_TYPE = 3;

    public static final Integer ERROR_LOG_CONSOLE= 1;
    public static final Integer ERROR_LOG_DATABASE = 2;
    public static final Integer ERROR_LOG_FILE = 3;

    public static final String KEY_MAP_USERNAME = "userName";
    public static final String KEY_MAP_PASSWORD = "password";
    public static final String KEY_MAP_DBMS = "dbms";
    public static final String KEY_MAP_SERVER_NAME = "serverName";
    public static final String KEY_MAP_PORT = "portNumber";
    public static final String KEY_MAP_FILE_FOLDER = "logFileFolder";


}
