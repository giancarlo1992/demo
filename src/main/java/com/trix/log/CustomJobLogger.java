package com.trix.log;

import com.trix.exception.LogException;
import com.trix.util.Constants;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;
import java.util.logging.*;

/**
 * Created by admin on 07/09/2017.
 */
public class CustomJobLogger {

    private static boolean logToFile;
    private static boolean logToConsole;
    private static boolean logMessage;
    private static boolean logWarning;
    private static boolean logError;
    private static boolean logToDatabase;
    private static Map dbParams;
    private static Logger logger;

    public CustomJobLogger(boolean logToFileParam, boolean logToConsoleParam, boolean logToDatabaseParam,
                     boolean logMessageParam, boolean logWarningParam, boolean logErrorParam, Map dbParamsMap) {
        logger = Logger.getLogger("MyCustomLog");
        logError = logErrorParam;
        logMessage = logMessageParam;
        logWarning = logWarningParam;
        logToDatabase = logToDatabaseParam;
        logToFile = logToFileParam;
        logToConsole = logToConsoleParam;
        dbParams = dbParamsMap;
    }

    public static void CustomLogMessage(String messageText) throws Exception {
        if(messageText!=null && messageText.trim().length() > 0){
            if (!logToConsole && !logToFile && !logToDatabase) {
                throw new Exception("Invalid configuration");
            }
            if ((!logError && !logMessage && !logWarning)) {
                throw new Exception("Error or Warning or Message must be specified");
            }
            if(logToDatabase)
                writeLogOnDatabase(messageText,getType());
            if(logToConsole)
                writeLogOnConsole(messageText);
            if(logToFile)
                writeLogOnFile(messageText,dbParams.get(Constants.KEY_MAP_FILE_FOLDER) + "/logFileNew.txt");
        }else{
            return;
        }
    }

    private static void writeLogOnDatabase(String message, Integer type){
        try {
            Connection connection = null;
            Properties connectionProps = new Properties();
            connectionProps.put("user", dbParams.get(Constants.KEY_MAP_USERNAME));
            connectionProps.put("password", dbParams.get(Constants.KEY_MAP_PASSWORD));
            connection = DriverManager.getConnection("jdbc:" + dbParams.get(Constants.KEY_MAP_DBMS) + "://" + dbParams.get(Constants.KEY_MAP_SERVER_NAME)
                    + ":" + dbParams.get(Constants.KEY_MAP_PORT) + "/", connectionProps);

            Statement stmt = connection.createStatement();
            stmt.executeUpdate("insert into Log_Values('" + message + "', " + String.valueOf(type) + ")");
        } catch (SQLException e) {
            throw new LogException(Constants.ERROR_LOG_DATABASE,getType(),"Error in Database cause by :" + e.getMessage());
        }
    }
    private static void writeLogOnFile(String messageText,String pathAndFile){
        try {
            File logFile = new File(pathAndFile);
            if (!logFile.exists())
                logFile.createNewFile();
            FileHandler fh = new FileHandler(pathAndFile);
            LogMessage(fh,messageText);
        }catch (IOException ie){
            throw new LogException(Constants.ERROR_LOG_FILE,getType(),"Cannot writing log file cause by :" + ie.getMessage());
        }
    }
    private static void writeLogOnConsole(String messageText){
        ConsoleHandler ch = new ConsoleHandler();
        LogMessage(ch,messageText);
    }
    private static void LogMessage(Handler h, String messageText){
        logger.addHandler(h);
        logger.log(Level.INFO, messageText);
    }
    private static Integer getType(){
        if (logMessage)
            return Constants.LOG_MESSAGE_TYPE;
        else if (logError)
            return Constants.LOG_ERROR_TYPE;
        else {
            return Constants.LOG_WARNING_TYPE;
        }
    }
}
